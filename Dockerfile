FROM alpine:edge
RUN apk --no-cache add unbound
COPY unbound.conf /etc/unbound/unbound.conf
COPY start.sh /start.sh
EXPOSE 53 53/udp
ENTRYPOINT ["/bin/sh"]
CMD ["/start.sh"]
